package com.idd.repository;

import com.idd.model.Product;
import org.springframework.data.repository.CrudRepository;

public interface ProductRepository extends CrudRepository <Product,Integer> {
}
