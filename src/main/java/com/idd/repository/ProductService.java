package com.idd.repository;

import com.idd.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service
public class ProductService {


    @Autowired
    ProductRepository productRepository;

    //getting all products record by using the method findAll() of CrudRepository
    public List<Product> getAllProducts()
    {
        List<Product> prods = new ArrayList<Product>();
        productRepository.findAll().forEach(prods::add);
        return prods;
    }

    //getting a specific record by using the method findById() of CrudRepository
    public Product getProductById(int id)
    {
        return productRepository.findById(id).get();
    }

    //saving a specific record by using the method save() of CrudRepository
    public void saveOrUpdate(Product product)
    {
        productRepository.save(product);
    }

    //deleting a specific record by using the method deleteById() of CrudRepository
    public void delete(int id)
    {
        productRepository.deleteById(id);
    }

    //updating a record
    public void update(Product product, int prodid)
    {
        productRepository.save(product);
    }


}
