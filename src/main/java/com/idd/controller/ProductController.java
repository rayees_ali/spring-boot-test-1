package com.idd.controller;

import com.idd.model.Product;
import com.idd.repository.ProductService;
import org.apache.catalina.connector.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ProductController {

    //autowire the ProductService class
    @Autowired
    ProductService productService;

    // retrieving all the products GET /products
    @GetMapping(path = "/allProducts", produces = "application/json")
    private ResponseEntity<List<Product>> getAllProducts() {
        return ResponseEntity.status(Response.SC_OK).body(productService.getAllProducts());
    }

    // retrieving specific product using prodid
    @GetMapping(path = "/prodById/{prodid}", produces = "application/json")
    private Product getProducts(@PathVariable("prodid") int prodid) {
        return productService.getProductById(prodid);
    }

    //creating a delete mapping that deletes a specified product
    @DeleteMapping("/deleteProd/{prodid}")
    private void deleteProduct(@PathVariable("prodid") int prodid) {
        productService.delete(prodid);
    }

    //creating post mapping that post the product detail in the database
    @PostMapping(path = "/addPruduct", consumes = "application/json", produces = "application/json")
    private int saveProduct(@RequestBody Product product) {
        productService.saveOrUpdate(product);
        return product.getId();
    }

    //creating put mapping that updates the product detail
    @PutMapping(path = "/updateProd", consumes = "application/json", produces = "application/json")
    private Product update(@RequestBody Product product) {
        productService.saveOrUpdate(product);
        return product;
    }


}
